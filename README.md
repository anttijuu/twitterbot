# TwitterBot

This is a Twitter bot implementation based on **twitter4j**.

When executed the first time, the bot will read the first line from the *Tweets.txt* file. The line number of the tweeted line is then saved into a file *currentline.txt*. 

Next time when bot is executed, the bot reads the line number from the *currentline.txt* file, and reads one line further from *Tweets.txt* and tweets that line.

When end of file is reached, the bot starts to tweet from the beginning of the file, if configured to do so. If you want to manually restart tweeting from the beginning of the file, just remove the *currentline.txt* file. You can also edit the text file with text editor to change where tweeting continues the next time.

You can configure the twitterbot by changing the settings in *twitterbot.properties* file (see below).

You can schedule the bot to run using *cron* in Linux ([info](https://help.ubuntu.com/community/CronHowto)) or *launchctl* in macOS ([info](https://developer.apple.com/library/content/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/CreatingLaunchdJobs.html)), for example. A sample plist file to be used with *launchctl* on macOS is provided (com.anttijuustila.karjalbotschedule.plist). The file schedules the bot to run every morning at 07:00 hrs to tweet once. Remember the limits twitter sets to the number of tweets you can update per day.

## Tweet file
Tweets.txt contains the tweets to post, one tweet per line. You might want to make the lines short enough to fit in a single tweet. However, if the tweet is longer, it will be sliced into several tweets and posted immediately, one after another.

Tweets.txt should be a plain text file, not a Word file or something else.

Each time the bot is run, the next line in the file is posted to Twitter.

### Scheduled tweets

If you wish, you can add a date in front of the tweet to indicate when it should be tweeted:

```{{2018-04-25}}This tweet is posted on April 25th, 2018, no earlier.```

Currently, you cannot specify a time of day, only date.

When the bot meets a line with a date, it checks if today is that date. If not, nothing is done nor the current line is advanced. Next time bot is run, it again does the same check with the same line of text in Tweets.txt. If the date is now, the tweet is posted and the current line is marked to be the next line. 

If you use these scheduled tweets, it would be best to schedule all the tweets in the file with the {{date}} mark. It is possible to insert both scheduled and non scheduled tweets in the Tweets.txt file, though. In this situation, the line is tweeted if there is no schedule, when the bot is run. If there is a scheduled tweet, and the date is in the future, no tweets in the file are posted until the date is reached, and the scheduled tweet is posted. Only then the next tweets (unscheduled or not) are handled.

Make sure you run the bot daily, at least once, to be able to post scheduled tweets.

## Dependencies

Java 1.8 was used in the development of this bot and Java runtime is needed to run it.

See [Twitter4j](http://twitter4j.org/en/index.html) for more information about Twitter4j.

TwitterBot assumes the *twitter4j-core.jar* is in the root directory of the TwitterBot when using the *build.sh* and *run.sh* shell scripts.

You will need to register your twitter bot app in [TwitterApps](https://apps.twitter.com) and use the keys from there to set the bot up. See Configuration below.

## Configuration

1. **Rename** the file *twitter4j-sample.properties* to twitter4j.properties, and **enter** your private twitter **keys and tokens** you got from apps.twitter.com for authorization.
    * For help and other means of authorization, see [Twitter4j Configuration](http://twitter4j.org/en/configuration.html) 
2. **Copy** the *twitter4j-core.jar* into the project root directory.
3. **Build** the project using the build.sh from terminal.
4. **Edit** the *twitterbot.properties* file to configure your bot:
    * set *twitterbot.debug=true* if you wish to test the bot without actually tweeting anything
    * set *twitterbot.restart=false* to stop tweeting when end of file is reached. Set it to true to continue tweeting from the beginning of *Tweets.txt* when end of file is reached.
5. **Edit** the *Tweets.txt* file to include your text to tweet, one tweet per line. Make sure the lines are not too long for a tweet.
    * Too long lines are split and tweeted as separate tweets though.
6. **Edit** the paths in the run.sh to match your environment.
7. **Run** the bot by entering a command *java -jar TwitterBot.jar* or using the run.sh from terminal or schedule it to run using cron/launchctl/something.

A tweet should be seen in the specified twitter account, each time you run the bot. If twitterbot.debug setting is true, tweets are visible in the console, but not actually tweeted.

The run.sh writes to *log.txt* you can view in case there are problems. Also, set *debug=true* in *twitter4j.properties* to aid problem solving, if needed. It will print out detailed twitter API error messages to console / log.txt file for investigation.

When you want to change the text to tweet, just replace the *Tweets.txt* file contents with new text, one tweet per line. Remember to remove the *currentline.txt* file to start tweeting from the first line.

## Who to contact

Antti Juustila, antti.juustila@gmail.com

This bot is published as-is without any warranties or conditions of any kind.

## License

Copyright 2018 Antti Juustila

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
