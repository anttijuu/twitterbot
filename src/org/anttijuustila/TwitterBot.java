package org.anttijuustila;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Tweets lines from a text file when executed.
 * Continues from the next line when executed again.
 * Starts over when reaching end of file.
 * 
 * @todo Logging to log file instead of System.out.
 * 
 * @author Antti Juustila
 */
public class TwitterBot implements Runnable {

	/** The line number of the line which was tweeted. */
	private int currentLine = 0;
	/** Should bot restart tweeting from beginning after EOF. */
	private boolean restartFromBeginning = false;
	/** In debugmode, does not tweet, only prints to console. */
	private boolean debugMode = false;
	/** Max length of the tweet. If line is longer, splits it to many tweets. */
	private final static int MAXTWEETLEN = 270;
	/** The text file where the tweets can be found. */
	private final static String TWEETFILE = "Tweets.txt";
	/** The file where the currently tweeted line is saved between bot executions. */
	private final static String CURRENTLINENUMFILE = "currentline.txt";

	private final static String DATE_COMPONENT_BEGIN = "{{";
	private final static String DATE_COMPONENT_END = "}}";
	private final static String DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * Creates and runs the bot.
	 * @param args Arguments not used.
	 */
	public static void main(String[] args) {
		new TwitterBot().run();
	}


	/**
	 * Executes the bot functionality.
	 * First, restores the state (line number), then reads lines until next line to tweet has been read.
	 * Then checks if the line is too long and must be split into shorter snippets.
	 * Then the text is tweeted.
	 */
	public void run() {
		try {			
			readConfiguration();
			restoreState();

			String line = getNextLine();

			if (null != line) {
				Twitter twitter = TwitterFactory.getSingleton();
				if (line.length() > 0) {
					if (line.length() > MAXTWEETLEN) {
						System.out.println(" >>>>> Too long a string, splitting...");
						List<String> parts = splitString(line, MAXTWEETLEN);
						for (String s : parts) {
							if (s.length() > 0) {
								tweetString(twitter, s);
							}
						}
					} else {
						tweetString(twitter, line);
					}
				}
				saveState();				
			}
		} catch (TwitterException e) {
			System.out.println("ERROR When trying to tweet!");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("ERROR No Tweets.txt file. Nothing to tweet.");
		} catch (IOException e) {
			System.out.println("ERROR in tweeting.");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("ERROR somewhere.");
			e.printStackTrace();
		}
	}

	private void tweetString(Twitter twitter, String s) throws TwitterException {
		Status status = null;
		if (debugMode) {
			System.out.println(currentLine + " DEBUG MODE NOT TWEETING: " + s);
		} else {
			status = twitter.updateStatus(s);
			System.out.println(currentLine + " successfully tweeted [" + status.getText() + "].");		
		}
	}

	private void readConfiguration() {
		File configFile = new File("twitterbot.properties");
		Properties config = new Properties();
		FileInputStream istream;
		try {
			istream = new FileInputStream(configFile);
			config.load(istream);
			if (config.getProperty("twitterbot.restart", "false").equalsIgnoreCase("true")) {
				restartFromBeginning = true;
			} else {
				restartFromBeginning = false;
			}
			if (config.getProperty("twitterbot.debug", "false").equalsIgnoreCase("true")) {
				debugMode = true;
			} else {
				debugMode = false;
			}
			istream.close();
		} catch (FileNotFoundException e) {
			System.out.println("ERROR No properties file to read settings from.");
		} catch (IOException e) {
			System.out.println("ERROR in reading settings from properties file.");
		}
	}


	/**
	 * Gets the next line to be tweeted.
	 * Ignores empty lines, trims whitespace from the string.
	 * 
	 * @return The string on the next line to tweet.
	 * @throws IOException
	 */
	private String getNextLine() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TWEETFILE));
		int counter = 0;
		String aLine = null;
		do {
			aLine = in.readLine();
			if (null != aLine) {
				aLine = aLine.trim();
				aLine = checkForTimedTweet(aLine);
				counter++;
			} else {
				if (currentLine > 0 && restartFromBeginning) {
					currentLine = -1;
					aLine = getNextLine();
					break;
				} else {
					// Line is null, eof reached, no restart so just break.
					System.out.println("EOF reached, not tweeting from this file anymore.");
					break; 
				}
			}
		} while (counter <= currentLine);
		in.close();
		currentLine++;
		return aLine;
	}


	private String checkForTimedTweet(String aLine) {
		if (aLine.startsWith(DATE_COMPONENT_BEGIN)) {
			System.out.println("Tweet has date component");
			String dateStr = aLine.substring(DATE_COMPONENT_BEGIN.length(), aLine.indexOf(DATE_COMPONENT_END));
			System.out.println("DateString is: " + dateStr);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
			LocalDate dateTime = LocalDate.parse(dateStr, formatter);
			LocalDate now = LocalDate.now();
			if (dateTime.compareTo(now) == 0) {
				System.out.println("Tweet should be updated today.");
				return aLine.substring(aLine.indexOf(DATE_COMPONENT_END)+DATE_COMPONENT_END.length());
			} else {
				System.out.println("Today is not the day for this tweet.");
				return null;
			}
		}
		return aLine;
	}


	/**
	 * Saves the state of the bot, that is the line number that was just tweeted.
	 */
	private void saveState() {
		try {
			File file = new File(CURRENTLINENUMFILE);
			FileWriter writer = new FileWriter(file);
			Integer i = new Integer(currentLine);
			String s = i.toString();
			writer.write(s);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Restores the state of the bot, that is the line number that was tweeted.
	 * If this fails, line number is set to zero, (start from beginning).
	 */
	private void restoreState() {
		Scanner s = null;
		try {	
			s = new Scanner(new BufferedReader(new FileReader(CURRENTLINENUMFILE)));     
			if (s.hasNextLine()) {
				String str = s.nextLine();
				Integer i = Integer.parseInt(str);
				currentLine = i.intValue();				
			}
			s.close();
		} catch (FileNotFoundException e) {
			currentLine = 0;
			System.out.println("No file to keep current line, will be saved later. Current line is 0");
		} catch (NumberFormatException e ) {
			currentLine = 0;
			System.out.println("Line number is not an integer.");
		} finally {
			if (null != s) {
				s.close();
			}
		}
	}

	// From http://www.davismol.net/2015/02/03/java-how-to-split-a-string-into-fixed-length-rows-without-breaking-the-words/
	/**
	 * Splits a too long a line to shorter parts for tweeting.
	 * @param msg The string to split.
	 * @param lineSize The max length of the line.
	 * @return List of strings short enough.
	 */
	private List<String> splitString(String msg, int lineSize) {
		List<String> res = new ArrayList<>();
		Pattern p = Pattern.compile("\\b.{1," + (lineSize-1) + "}\\b\\W?");
		Matcher m = p.matcher(msg);

		while (m.find()) {
			res.add(m.group());
		}
		return res;
	}

}
