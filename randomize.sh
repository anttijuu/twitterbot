@echo off
setlocal EnableExtensions DisableDelayedExpansion

set /A "index=0"
for /f "delims=" %%a in ('findstr /N /R "^" "%~dpn0.lst"') do (
    setlocal EnableDelayedExpansion
    for /F %%b in ("$$!random!.!index!") do (
        endlocal
        set "%%b=%%a"
    )
    set /A "index+=1"
)
> "%~dpn0.new" (
    for /f "delims=" %%a in ('set $$') do (
        set "item=%%a"
        setlocal EnableDelayedExpansion
        echo(!item:*:=!
        endlocal
    )
)

endlocal
exit /B

